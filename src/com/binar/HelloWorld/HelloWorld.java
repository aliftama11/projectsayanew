package com.binar.HelloWorld;

public class HelloWorld {
// komen 1 line
    /*
    lebih dari 1 baris
    komennya
     */

    /*
    1. method void : tidak mengembalikan nilai apapun
    2. method return : akan mengembalikan nilai berdasarkan tipe data yang digunakan
     */
    public static void segitiga(int alas,int tinggi){
        System.out.println("luas segitiga "+(alas*tinggi)/2);
    }
    public static void main(String[] args){
        segitiga(1,6);
    }
}
